FROM node:13.8.0

ENV HOME=/home/app

COPY . $HOME

WORKDIR $HOME
RUN npm i -g @adonisjs/cli
RUN npm install

EXPOSE 3333
CMD [ "npm", "start" ]
