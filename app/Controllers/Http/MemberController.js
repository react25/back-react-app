'use strict'
const Member =use('App/Models/Member')

class MemberController {
  async index(){
    return await Member.all()
  }

  async indexBis(){
    return await Member.all()
  }

  async show ({params}) {
    return await Member.find(params.id)
  }

  async store ({ request, response }) {

    const memberInfo = request.only(['first_name' , 'last_name', 'email', 'city'])

    const memberExists = await Member.findBy('email', memberInfo.email)

    if(memberExists) {
      return response.status(400).json({
        status: 'error',
        message: 'Email already exist please choose another'
      })
    }else {
      let member = await Member.create(memberInfo);
      return response.status(200).json({
        member: member,
        status: 'succes',
        message: 'Member adding'
      })
    }
  }

  async update ({ params, request, response }) {
    try {
      const first_name = request.input('first_name');
      const last_name = request.input('last_name');
      const email = request.input('email');
      const city = request.input('city');
      let member = await Member.find(params.id);

      member.first_name = first_name;
      member.last_name = last_name;
      member.email = email;
      member.city = city;
      await member.save();
      return response.status(200).json({
        member: member,
        status:'succes',
        message:'member update '
      })
    }catch (error) {
      return response.status(400).json({
        status: 'error',
        message: 'There was a problem updating member, please try again later.'
      })

    }
  }


  async delete ({ params,response}) {
    try {

      const member =await Member.find(params.id)
      await member.delete();
      return response.status(200).json({
        status:'succes',
        message: 'Member deleted!'
      })
    }catch(error) {
      return response.status(400).json({
        status: 'error',
        message: 'There was a problem deleting member, please try again later.'
      })
    }
  }

}
module.exports = MemberController
