'use strict'
const User = use('App/Models/User');

class AuthController {
  async login ({ request, auth, response }) {
    let {email, password} = request.all();
      try {

        if (await auth.attempt(email, password)) {
           let user = await User.findBy('email', email)
           let accessToken = await auth.generate(user,true)
           Object.assign(user, accessToken)
           return response.json({
                status: 'success',
                accessToken: accessToken
            })
        }
      } catch (e) {
          response.status(400).json({
              status: 'error',
              message: 'Invalid email/password',
              email: email,
              user: user
          })
      }
    }
    // async register({request, auth, response}) {
    //     let user = await User.create(request.all())
    //
    //     let accessToken = await auth.generate(user)
    //     Object.assign(user, accessToken)
    //     return response.json({"user": user, "access_token": accessToken})
    // }



}

module.exports = AuthController
