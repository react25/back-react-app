# Adonis api

# simple crud members  

# Ubuntu nginx reverse Proxy server configuration

### list

- [x] nginx
- [x] node 15
- [x] git
- [x] nano
- [x] gcc
- [x] build-essential
- [x] pm2

## Node installation using a PPA

`$ sudo apt-get update`
`$ cd ~`
`$ curl -sL https://deb.nodesource.com/setup_15.x -o nodesource_setup.sh`
`$ sudo bash nodesource_setup.sh`
`$ sudo apt-get install -y nodejs`

## Installation gcc

`$ sudo apt-get install gcc g++ make`

## Installation build-essential

`$ sudo apt install build-essential`

## Installation git

`$ sudo apt-get install git`

## Installation nano

`$ sudo apt-get install nano`

## Installation nginx
[Nginx](https://nginx.org/en/)

`$ sudo apt-get install nginx -y`

Status check `$ sudo systemctl status nginx`

Launching nginx at system startup `$ sudo systemctl start nginx`

Configuration check `$ sudo nginx -t`

Restart the nginx server to apply the configuration changes `$ sudo systemctl reload nginx`

Allow Nginx Traffic `$ sudo ufw enable`

`$ sudo ufw allow 'Nginx Full'`
`$ sudo ufw allow 'OpenSSH'`
`$ sudo ufw status`

## Installation PM2
`$ sudo npm install pm2 -g`
[PM2](https://pm2.keymetrics.io/)

## Installation project adonis
You can clone your projet or install adonis
[Adonis setup](https://adonisjs.com/docs/4.1/installation)
Change config proxy  
[Adonis proxy setup](https://adonisjs.com/recipes/4.1/nginx-proxy)
And finally
Don’t forget to restart Nginx server with the following command `$ sudo service nginx restart`

Enjoy :smiley:
