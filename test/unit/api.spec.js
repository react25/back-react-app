'use strict'
const Member =use('App/Models/Member');
const { trait,test,before,after} = use('Test/Suite')('Api');
trait('Test/ApiClient');

before(async () => {
    await Member.createMany([
        {id: 1, first_name: 'Marmoud', last_name: 'Jean',email:'JMarmoud@test.com' , city:'Paris'},
        {id: 2, first_name: 'Marmoud2', Last_name: 'Jean2',email:'JMarmoud2@test.com' , city:'Paris2'},
        {id: 3, first_name: 'Marmoud3', Last_name: 'Jean3',email:'JMarmoud3@test.com' , city:'Paris3'},
    ])

})

after(async () => {
    await Member.query().delete()

})

test('get list of members', async({client})=>{
  const response = await client
    .get('/members').end()
    response.assertStatus(200);
    response.assertJSONSubset([
    {
      first_name:"Marmoud",
      last_name:"Jean",
      email:"JMarmoud@test.com",
      city: "Paris"
    },
    {
      first_name:"Marmoud2",
      last_name:"Jean2",
      email:"JMarmoud2@test.com",
      city: "Paris2"
    },
    {
      first_name:"Marmoud3",
      last_name:"Jean3",
      email:"JMarmoud3@test.com",
      city: "Paris3"
    }

  ])
});

test('get Member by Id', async({client})=>{
  const response = await client
    .get('/members/1').end()
    response.assertStatus(200);
    response.assertJSONSubset(
    {
      first_name:"Marmoud",
      last_name:"Jean",
      email:"JMarmoud@test.com",
      city: "Paris"
    }
  )
});

test('members update (put)', async ({client}) => {
    const response = await client
        .put('/members/1')
        .send({first_name: 'Marmoud', last_name: 'Jean',email:'JMarmoud3@test.com' , city:'Lyon'})
        .end()
        response.assertStatus(200)
        response.assertJSONSubset(
        {
          status:'succes',
          message:'member update ',
            member: {
                first_name: 'Marmoud',
                last_name: 'Jean',
                email: 'JMarmoud3@test.com',
                city: 'Lyon',
            }
          }
    )
})
test('members update exception (put)', async ({client}) => {
    const response = await client
        .put('/members/5')
        .send({})
        .end()
        response.assertStatus(400)
        response.assertJSONSubset(
        {
          status: 'error',
          message: 'There was a problem updating member, please try again later.'
        })

})
test('members add (post)', async ({ client }) => {
  const response = await client
      .post('/members')
      .send({first_name: 'Springfellow', last_name: 'Elena',email:'ESpringfellow@test.com' , city:'Colorado'})
      .end()
        // console.log(response)
      response.assertStatus(200)
      response.assertJSONSubset({
        status: 'succes',
        message: 'Member adding'
      })
})

test('members add exception (post)', async ({ client }) => {
  const response = await client
      .post('/members')
      .send({first_name: 'Marmoud', last_name: 'Jean',email:'JMarmoud3@test.com' , city:'Lyon'})
      .end()
      response.assertStatus(400)
      response.assertJSONSubset({
        status: 'error',
        message: 'Email already exist please choose another'
      })
})
test('members delete  (delete)', async ({client}) => {
    const response = await client
        .delete('/members/3')
        .send({})
        .end()
        response.assertStatus(200)
        response.assertJSONSubset(
        {
          status:'succes',
          message: 'Member deleted!'
        })
})

test('members delete exception (delete)', async ({client}) => {
    const response = await client
        .delete('/members/5')
        .send({})
        .end()
        response.assertStatus(400)
        response.assertJSONSubset(
        {
          status: 'error',
          message: 'There was a problem deleting member, please try again later.'
        })
})
