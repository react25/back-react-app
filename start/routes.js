'use strict'
const Member = use('App/Models/Member')
const User = use('App/Models/User')
/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => 'apiDashboard v1')

Route.put('members/:id', 'MemberController.update');
Route.delete('members/:id', 'MemberController.delete');
Route.post('members', 'MemberController.store');
Route.get('members', 'MemberController.index')
Route.get('members/:id', 'MemberController.show');
// Route.get('membersBis', 'MemberController.indexBis').middleware(['auth:jwt']);

Route.post('/login', 'AuthController.login')
// Route.post('/register', 'AuthController.register')
