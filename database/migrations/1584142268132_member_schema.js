'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MemberSchema extends Schema {
  up () {
    this.create('member', (table) => {
      table.increments()
      table.string('first_name',80).notNullable()
      table.string('last_name',80).notNullable()
      table.string('email',254).notNullable()
      table.string('city', 254).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('member')
  }
}

module.exports = MemberSchema
